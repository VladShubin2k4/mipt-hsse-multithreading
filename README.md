# Избранные материалы курсов "ТиПМС" и "Параллельные и распределённые вычисления" ВШПИ МФТИ

## Общая информация

Автор реализаций: Шубин Владислав Андреевич

### Материалы

[Параллельные и распределённые вычисления](https://disk.yandex.ru/d/OXw7azQtXMpdew)

Подробнее про технологии параллелизма вычислений:

- [CUDA](https://enccs.github.io/cuda/)
- [MPI](https://enccs.github.io/intermediate-mpi/)

## Навигация

### Реализации по каждой из тем находятся на соответствущей ветке

- [MPI](https://gitlab.com/VladShubin2k4/mipt-hsse-multithreading/-/tree/MPI?ref_type=heads)
- [OMP](https://gitlab.com/VladShubin2k4/mipt-hsse-multithreading/-/tree/OMP?ref_type=heads)
- [Concurrency](https://gitlab.com/VladShubin2k4/mipt-hsse-multithreading/-/tree/concurrency?ref_type=heads)

## Для сборки через CMake необходимо прописать данный код в CMakeLists.txt:
```
find_package(MPI)       #REQUIRED
find_package(OpenMP)    #REQUIRED

target_link_libraries(
        <target>

        PUBLIC
        
        MPI::MPI_CXX
        OpenMP::OpenMP_CXX
)
```